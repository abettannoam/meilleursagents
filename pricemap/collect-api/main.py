from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import requests

# DATABASE configs
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///./sql_app.db"
# app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql+psycopg2://pricemap:pricemap@db:5432/pricemap/"


# To delete somes warnings
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


# Models
class Annoncement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    listing_id = db.Column(db.String, unique=False)
    place_id = db.Column(db.String, unique=False, nullable=False)
    price = db.Column(db.String, nullable=False)
    area = db.Column(db.Float, nullable=False)
    room_count = db.Column(db.Integer, nullable=False)


# Localhost
DISTRICTS_ID = [str(item) for item in list(range(32682, 32702))]
base_listings_url = "http://localhost:8181/listings/"


# Create database
db.create_all()

# Pages loop over discrits
page = 0
average_price=0

# For all districts...
for district in DISTRICTS_ID:
    while True:

        r = requests.get(base_listings_url + district + "?page=" + str(page))

        if r.status_code != 200:
            print("District:", district)
            print("page number", page)
            with open('file.txt', 'a') as fp:
                fp.write(f"{district} {average_price / (float(page)*20)}\n")
            average_price=0
            page=0
            break

        # Do something here
        for item in r.json():

            # Adapting format for area and room_count
            try:
                room_count, area = item['title'].encode('ascii', 'ignore').decode('utf8').split('-')
            except:
                pass

            if isinstance(room_count, str):
                room_count = 1 if room_count[0] == 'Studio ' else room_count.split()

            if isinstance(room_count, list):
                if len(room_count) == 1:
                    room_count = 1
                else:
                    room_count = room_count[1][0]

            price = item['price'].encode('ascii', 'ignore').decode('utf8')

            annonce = Annoncement(listing_id=item['listing_id'],
                                  place_id=district,
                                  price=price,
                                  area=area[:-1],
                                  room_count=room_count)

            # On met a jour le prix moyen pour chaque district
            try:
                average_price += (float(price) / float(area[:-1]))
            except Exception as err:
                print(err)

            db.session.add(annonce)
            db.session.commit()

        # Page increment
        page += 1

# We can filter database
annonces = Annoncement.query.filter_by(place_id='32682').all()
print(annonces)
